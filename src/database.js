const mongoose = require('mongoose');
require('dotenv').config();



const config = {
  'url': process.env.MONGO_URI,
  'localUrl': 'mongodb://localhost/xfit'
}

mongoose.connect(config.localUrl ,{ useNewUrlParser: true } , (err, response) => {
  if (err) throw err;
  console.log('BD online');
});

module.exports = mongoose.connection;
