const mongoose = require('mongoose');
const { Schema } = mongoose;
const timestamp = require('mongoose-timestamp');

const paymentSchema = new Schema({
  clientId: {type: Schema.Types.ObjectId, required: true},
  amount: {type: Number, required: true},
  deathLine: {type: String}
});
paymentSchema.plugin(timestamp);

module.exports = mongoose.model('payment', paymentSchema);