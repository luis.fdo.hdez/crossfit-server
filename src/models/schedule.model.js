const mongoose = require('mongoose');
const { Schema } = mongoose;
const timestamp = require('mongoose-timestamp');

const scheduleSchema = new Schema({
  client: { type: Schema.Types.ObjectId, required: true, ref: 'user'},
  date: { type: String, required: true },
  time: { type: String, required: true},
  status: {type: Number} // agendada: 0, tomada: 1, falta: 2
});

scheduleSchema.plugin(timestamp);

module.exports = mongoose.model('schedule', scheduleSchema);