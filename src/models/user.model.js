const mongoose = require('mongoose');
const { Schema } = mongoose;
const timestamp = require('mongoose-timestamp');

const validRoles = ['admin', 'manager', 'client'];

const userSchema = new Schema({
  name: {type: String},
  email: {type: String, required: true},
  password: {type: String, required: true},
  role: {type: String, enum: validRoles, default: 'client'},
  phone: {type: String},
  deathLine: {type: String}
});
userSchema.plugin(timestamp);

userSchema.path('email').validate( async (value) => {
  const emailCount = await mongoose.models.user.countDocuments({email: value});
  return !emailCount;
}, 'Email already exists');

module.exports = mongoose.model('user', userSchema);
