const express = require('express');
const permissions = require('../utils/permissions');
const router = express.Router();
const user = require('../controllers/user.controller');

router.post('/new', user.createUser);
router.put('/changePass', permissions.isLoggedIn, user.changePass);
router.get('/all', user.getUsers); // poner permisos de admin
module.exports = router;
