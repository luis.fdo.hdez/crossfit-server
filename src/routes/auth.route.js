const express = require('express');

const router = express.Router();
const auth = require('../controllers/auth.controller');

router.post('/client', auth.login);
router.post('/trainer', auth.loginAsTrainer);
router.get('/verify', auth.verify);
router.get('/logout', auth.logout);
router.get('/updateSchedule', auth.scheduleVerify);

module.exports = router;
