const express = require('express');
const permissions = require('../utils/permissions');
const router = express.Router();

const reports = require('../controllers/reports.controller');

router.get('/usersByMonth', reports.usersByMonth);
router.get('/paymentsByMonth', reports.paymentsByMonth);

module.exports = router;