const express = require('express');
const permissions = require('../utils/permissions');
const router = express.Router();

const payment = require('../controllers/payment.controller');

router.post('/new', payment.newPayment);

module.exports = router;