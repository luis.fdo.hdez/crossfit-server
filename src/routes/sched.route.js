const express = require('express');
const permissions = require('../utils/permissions');
const router = express.Router();
const schedule = require('../controllers/schedule.controller');

router.post('/new', permissions.isLoggedIn, schedule.createSchedule);
router.get('/all/:id', schedule.getSchedByUser);
router.get('/admin/:id', permissions.isLoggedIn, schedule.getForAdminByUser);
router.get('/history', permissions.isLoggedIn, schedule.getHistory);
router.get('/noattendance', permissions.isLoggedIn, schedule.getNotAttendance);
router.put('/:id', schedule.editSched);
router.get('/usersByDate/:date', schedule.usersByDate);
module.exports = router;