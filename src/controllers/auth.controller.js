const User = require('../models/user.model');
const bcrypt = require('bcrypt');
const rounds = 10;
const Schedule = require('../models/schedule.model');
const moment = require('moment');

const authCtrl = { };

authCtrl.login = async (req, res) => {
  let user = await User.findOne({email: req.body.email});
  let samePass = await bcrypt.compare(req.body.password, user.password);
  if (!samePass) {
    res.status(401).end();
  } else {
    req.session.user = user.toObject();
    res.status(200).json(user);
  }
}

authCtrl.loginAsTrainer = async (req, res) => {
  let user = await User.findOne({email: req.body.email, role: 'manager'});
  if (!user) {
    res.status(401).json({
      message: 'Debes ser entrenador para ingresar por aquí'
    });
  } else {
    let samePass = await bcrypt.compare(req.body.password, user.password);
    if (!samePass) {
      res.status(401).end();
    } else {
      req.session.user = user.toObject();
      res.status(200).json(user);
    }
  }
}

authCtrl.verify = async (req, res) => {
  try {
    let user = await User.findOne({email: req.session.user.email});
    res.json(user);
  } catch(err) {
    res.status(400).send(err.message);
  }
}

authCtrl.logout = async (req, res) => {
  let logout = await req.session.destroy();
  if (logout) {
    res.json(false);
  }
}

authCtrl.scheduleVerify = async (req, res) => {
  try {
    let arr = [];
    let user = await User.findOne({email: req.session.user.email});
    let schedules = await Schedule.find({client: user._id}); // 
    today = new Date().getTime();
    schedules.forEach(element => {
      element.date = new Date(element.date).getTime();
      if (today > element.date && element.status === 0) {
        Schedule.findOneAndUpdate(element._id, {$set: {status: 1}}, {new: true}, (err, updated) => {
          return updated;
        });
      }
    });
    res.json(user);
  } catch(err) {
    res.status(500).json(err.message);
  }
}


module.exports = authCtrl;