const User = require('../models/user.model'),
      Payment = require('../models/payment.model');
const moment = require('moment');
_ = require('underscore');

const reportsCtrl = { };

reportsCtrl.usersByMonth = function (req, res) {
  let months = [];
  let result = [];
  User.find().exec(function(err, users) {
    _.each(users, function(user) {
      const userMonth = moment(user.createdAt).format('MM/YYYY');
      months.push({
        month: userMonth,
        users: user
      });
    });
    var reducted = (months, value) => {
      let i = 0;
      while (i < months.length) {
        if (months[i].month === value) return i;
        i++;
      }
      return false;
    }
    months.forEach((e) => {
      let i = reducted(result, e.month);
      if (i === false) {
        result.push({
          month: e.month,
          users: [e.users]
        });
      } else {
        result[i].users.push(e.users);
      }
    });
    res.send(result);
  });
}

reportsCtrl.paymentsByMonth = function(req, res) {
  let months = [];
  let result = [];
  Payment.find().exec(function(err, payments) {
    _.each(payments, function(user) {
      const userMonth = moment(user.createdAt).format('MM/YYYY');
      months.push({
        month: userMonth,
        payments: user
      });
    });
    var reducted = (months, value) => {
      let i = 0;
      while (i < months.length) {
        if (months[i].month === value) return i;
        i++;
      }
      return false;
    }
    months.forEach((e) => {
      let i = reducted(result, e.month);
      if (i === false) {
        result.push({
          month: e.month,
          payments: [e.payments]
        });
      } else {
        result[i].payments.push(e.payments);
      }
    });
    res.send(result);
  })
}

module.exports = reportsCtrl;