const User = require('../models/user.model');
const bcrypt = require('bcrypt');
const rounds = 10;

const userCtrl = { };

userCtrl.createUser = async (req, res) => {
  let body = req.body;
  let hashPass = await bcrypt.hash(body.password, rounds);
  let user = await new User({
    name: body.name,
    email: body.email,
    role: body.role,
    password: hashPass,
    phone: body.phone
  });
  try {
    let newUser = await user.save();
    req.session.user = newUser;
    res.json(newUser);
  } catch(err) {
    res.json(err.message);
  }
}

userCtrl.changePass = async (req, res) => {
  user = req.session.user;
  try {
    let samePass = await bcrypt.compare(req.body.oldPassword, user.password);
    if (!samePass) {
      res.status(401).end();
    } else {
      let updated = await User.findOneAndUpdate({_id: user._id}, {$set: {password: req.body.newPassword}});
      res.json({message: 'User updated'});
    }
  } catch(err) {
    res.status(500).json(err.message);
  }
}

userCtrl.getUsers = async (req, res) => {
  try {
    const users = await User.find({role: 'client'});
    res.json(users);
  } catch(err) {
    res.status(404).json('Users not found');
  }
}
module.exports = userCtrl;
