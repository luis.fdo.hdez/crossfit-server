const User = require('../models/user.model'),
      Payment = require('../models/payment.model');

const paymentCtrl = { };

paymentCtrl.newPayment = async (req, res) => {
  try {
    let payment = await new Payment(req.body).save();
    let user = await User.findOneAndUpdate({_id: req.body.clientId}, {$set: {deathLine: req.body.deathLine}}, {new: true});
    res.json({payment, user});
  } catch(err) {
    res.status(500).json(err.message);
  }
}

module.exports = paymentCtrl;