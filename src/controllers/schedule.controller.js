const Schedule = require('../models/schedule.model');
const moment = require('moment');
const schedCtrl = { };

schedCtrl.createSchedule = async (req, res) => {
  const sched = await new Schedule({
    client: req.session.user._id,
    date: req.body.date,
    time: req.body.time,
    status: 0
  });
  try {
    const newSched = await sched.save();
    res.json(newSched);
  } catch(err) {
    res.status(500).json(err.message);
  }
}

schedCtrl.getForAdminByUser = async (req, res) => {
  try {
    let scheds = await Schedule.find({client: req.params.id});
    res.json(scheds);
  } catch(err) {
    res.status(500).json(err.message);
  }
}

schedCtrl.getSchedByUser = async (req, res) => {
  try {
    let scheds = await Schedule.find({client: req.params.id, status: 0});
    res.json(scheds);
  } catch(err) {
    res.status(500).json(err.message);
  }
}

schedCtrl.getHistory = async (req, res) => {
  try {
    let scheds = await Schedule.find({client: req.session.user._id, status: 1});
    res.json(scheds);
  } catch(err) {
    res.status(500).json(err.message);
  }
}

schedCtrl.getNotAttendance = async (req, res) => {
  try {
    let scheds = await Schedule.find({client: req.session.user._id, status: 2});
    res.json(scheds);
  } catch(err) {
    res.status(500).json(err.message);
  }
}

schedCtrl.editSched = async (req, res) => {
  const { id } = req.params;
  const schedule = {
    client: req.body.client,
    date: req.body.date,
    time: req.body.time
  };
  const updatedSched = await Schedule.findOneAndUpdate(id, {$set: schedule}, {new: true});
  res.json(updatedSched);
}

schedCtrl.usersByDate = async (req, res) => {
  try {
    let { date } = req.params;
    str = date.replace(/-/g, '/');
    const scheduleds = await Schedule.find({date: str})
    .populate('client', 'name email');
    res.json(scheduleds);

  } catch(err) {
    res.status(500).json(err.message);
  }
}
module.exports = schedCtrl;