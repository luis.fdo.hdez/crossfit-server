const express = require('express');
const cors = require('cors');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const path = require('path');
const db = require('./database');
require('dotenv').config()
const app = express();

// Settings
app.set('port', process.env.PORT || 3000);
app.use(express.static(path.join(__dirname, 'public')));

// Midlewares
app.use(session({
  secret: '0nf082hf82fho2hy3f982y)&)G',
  resave: false,
  saveUninitialized: false,
  cookie: {
    secure: false,
    maxAge: 3 * 24 * 60 * 60 * 1000,
  },
  store: new MongoStore({
    mongooseConnection: db,
    ttl: 14 * 24 * 60 * 60, // = 14 days. Default
  }),
}));

app.use(cors());
app.options('*', cors());
app.use(express.json());
// routes
app.use('/api/users', require('./routes/user.route'));
app.use('/api/auth', require('./routes/auth.route'));
app.use('/api/schedule', require('./routes/sched.route'));
app.use('/api/payments', require('./routes/payment.route'));
app.use('/api/reports', require('./routes/reports.route'));

// start server
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});

app.listen(app.get('port'), () => {
  console.log('server on port ', app.get('port'));
});