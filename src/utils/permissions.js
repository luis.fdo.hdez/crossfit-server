module.exports = {
  isAdmin: (req, res, next) => {
    if (req.session.user.role === 'admin') {
      next();
    } else {
      res.end();
    }
  },
  isLoggedIn: (req, res, next) => {
    if (req.session.user === undefined || req.session.user === null) {
      res.status(401).end();
    } else {
      next();
    }
  },
};
